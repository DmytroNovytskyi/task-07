package com.epam.rd.java.basic.task7.db;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.*;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Properties;

import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {

    private static final String FULL_URL;

    private static DBManager instance;

    public static synchronized DBManager getInstance() {
        if (instance == null) {
            instance = new DBManager();
        }
        return instance;
    }

    private DBManager() {
    }

    public List<User> findAllUsers() throws DBException {
        List<User> users = new ArrayList<>();
        try (Connection connection = DriverManager.getConnection(FULL_URL);
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery("SELECT * FROM users")) {
            int i;
            while (resultSet.next()) {
                i = 0;
                User user = new User();
                user.setId(resultSet.getInt(++i));
                user.setLogin(resultSet.getString(++i));
                users.add(user);
            }
        } catch (SQLException e) {
            throw new DBException("findAllUsers db error", e);
        }
        return users;
    }

    public boolean insertUser(User user) throws DBException {
        try (Connection connection = DriverManager.getConnection(FULL_URL);
             PreparedStatement statement = connection.prepareStatement("INSERT INTO users VALUES(DEFAULT, ?)",
                     Statement.RETURN_GENERATED_KEYS)) {
            statement.setString(1, user.getLogin());
            statement.execute();
            try (ResultSet resultSet = statement.getGeneratedKeys()) {
                resultSet.next();
                user.setId(resultSet.getInt(1));
            } catch (SQLException e) {
                throw new DBException("resultSet in insertUser db error", e);
            }
            return true;
        } catch (SQLException e) {
            throw new DBException("insertUser db error", e);
        }
    }

    public boolean deleteUsers(User... users) throws DBException {
        Connection connection = null;
        PreparedStatement statement = null;
        int updated = 0;
        try {
            connection = DriverManager.getConnection(FULL_URL);
            connection.setAutoCommit(false);
            connection.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
            statement = connection.prepareStatement("DELETE FROM users WHERE login = ?");
            for (User u :
                    users) {
                statement.setString(1, u.getLogin());
                updated += statement.executeUpdate();
            }
            connection.commit();
            if (updated != 0) {
                return true;
            }
            return false;
        } catch (SQLException e) {
            rollback(connection);
            throw new DBException("deleteUsers db error", e);
        } finally {
            close(statement);
            close(connection);
        }
    }

    public User getUser(String login) throws DBException {
        User user = null;
        try (Connection connection = DriverManager.getConnection(FULL_URL);
             PreparedStatement statement = connection.prepareStatement("SELECT * FROM users WHERE login = ?")) {
            statement.setString(1, login);
            statement.execute();
            try (ResultSet resultSet = statement.getResultSet()) {
                int i = 0;
                if (resultSet.next()) {
                    user = new User();
                    user.setId(resultSet.getInt(++i));
                    user.setLogin(resultSet.getString(++i));
                }
                return user;
            } catch (SQLException e) {
                throw new DBException("resultSet in getUser db error", e);
            }
        } catch (SQLException e) {
            throw new DBException("getUser db error", e);
        }
    }

    public Team getTeam(String name) throws DBException {
        Team team = null;
        try (Connection connection = DriverManager.getConnection(FULL_URL);
             PreparedStatement statement = connection.prepareStatement("SELECT * FROM teams WHERE name = ?")) {
            statement.setString(1, name);
            statement.execute();
            try (ResultSet resultSet = statement.getResultSet()) {
                int i = 0;
                if (resultSet.next()) {
                    team = new Team();
                    team.setId(resultSet.getInt(++i));
                    team.setName(resultSet.getString(++i));
                }
                return team;
            } catch (SQLException e) {
                throw new DBException("no team was found", e);
            }
        } catch (SQLException e) {
            throw new DBException("getUser db error", e);
        }
    }

    public List<Team> findAllTeams() throws DBException {
        try (Connection connection = DriverManager.getConnection(FULL_URL);
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery("SELECT * FROM teams")) {
            return mapTeams(resultSet);
        } catch (SQLException e) {
            throw new DBException("findAllTeams db error", e);
        }
    }

    public boolean insertTeam(Team team) throws DBException {
        try (Connection connection = DriverManager.getConnection(FULL_URL);
             PreparedStatement statement = connection.prepareStatement("INSERT INTO teams VALUES(DEFAULT, ?)",
                     Statement.RETURN_GENERATED_KEYS)) {
            statement.setString(1, team.getName());
            statement.execute();
            try (ResultSet resultSet = statement.getGeneratedKeys()) {
                resultSet.next();
                team.setId(resultSet.getInt(1));
            } catch (SQLException e) {
                throw new DBException("resultSet in insertTeam db error", e);
            }
            return true;
        } catch (SQLException e) {
            throw new DBException("insertTeam db error", e);
        }
    }

    public boolean setTeamsForUser(User user, Team... teams) throws DBException {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = DriverManager.getConnection(FULL_URL);
            connection.setAutoCommit(false);
            connection.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
            statement = connection.prepareStatement("INSERT INTO users_teams VALUES (?, ?)");
            statement.setInt(1, user.getId());
            for (Team team :
                    teams) {
                statement.setInt(2, team.getId());
                statement.execute();
            }
            connection.commit();
            return true;
        } catch (SQLException e) {
            rollback(connection);
            throw new DBException("setTeamsForUser db error", e);
        } finally {
            close(statement);
            close(connection);
        }
    }

    public List<Team> getUserTeams(User user) throws DBException {
        try (Connection connection = DriverManager.getConnection(FULL_URL);
             PreparedStatement statement = connection.prepareStatement("SELECT t.id, t.name " +
                     "FROM teams t " +
                     "INNER JOIN users_teams ut " +
                     "ON t.id = ut.team_id " +
                     "WHERE ut.user_id = ?")) {
            statement.setInt(1, user.getId());
            statement.execute();
            try (ResultSet resultSet = statement.getResultSet()) {
                return mapTeams(resultSet);
            } catch (SQLException e) {
                throw new DBException("resultSet in getUserTeams db error", e);
            }
        } catch (SQLException e) {
            throw new DBException("getUserTeams db error", e);
        }
    }

    public boolean deleteTeam(Team team) throws DBException {
        try (Connection connection = DriverManager.getConnection(FULL_URL);
             PreparedStatement statement = connection.prepareStatement("DELETE FROM teams t WHERE t.name = ?")) {
            statement.setString(1, team.getName());
            statement.execute();
            return true;
        } catch (SQLException e) {
            throw new DBException("deleteTeam db error", e);
        }
    }

    public boolean updateTeam(Team team) throws DBException {
        try (Connection connection = DriverManager.getConnection(FULL_URL);
             PreparedStatement statement = connection.prepareStatement("UPDATE teams " +
                     "SET name = ? " +
                     "WHERE id = ?")) {
            statement.setString(1, team.getName());
            statement.setInt(2, team.getId());
            if (statement.executeUpdate() != 0) {
                return true;
            }
            return false;
        } catch (SQLException e) {
            throw new DBException("updateTeam db error", e);
        }
    }

    private void rollback(Connection connection) {
        if (connection != null) {
            try {
                connection.rollback();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }

    private void close(AutoCloseable ac) {
        if (ac != null) {
            try {
                ac.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private List<Team> mapTeams(ResultSet resultSet) throws SQLException {
        int i;
        List<Team> teams = new ArrayList<>();
        while (resultSet.next()) {
            i = 0;
            Team team = new Team();
            team.setId(resultSet.getInt(++i));
            team.setName(resultSet.getString(++i));
            teams.add(team);
        }
        return teams;
    }
    static {
            Properties properties = new Properties();
            try (InputStream in = Files.newInputStream(Paths.get("app.properties"))) {
                properties.load(in);
            } catch (IOException exception) {
                System.err.println("Cannot find file.");
            }
            FULL_URL = properties.getProperty("connection.url");
    }
}
